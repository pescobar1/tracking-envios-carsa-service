package com.carsa.tracking.entidades;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Clase que contiene una lista de Estados asociados a un Envio.
 * 
 * Es utilizada para enviar como respuesta a la solicitud del Traking Historico de un Envio.
 * 
 * */
@JsonAutoDetect
public class EnvioTrackingResponse {
	
	private String numeroEnvio;
	//private String situacion;
	private List<EventoTracking> eventos= new ArrayList<EventoTracking>();

	
	public String getNumeroEnvio() {
		return numeroEnvio;
	}
	public void setNumeroEnvio(String numeroEnvio) {
		this.numeroEnvio = numeroEnvio;
	}
	public List<EventoTracking> getEventos() {
		return eventos;
	}
	public void setEventos(List<EventoTracking> eventos) {
		this.eventos = eventos;
	}
		
//	public String getSituacion() {
//		return situacion;
//	}
//	public void setSituacion(String situacion) {
//		this.situacion = situacion;
//	}
	@Override
	public String toString() {
		return "EnvioTrackingResponse [numeroEnvio=" + numeroEnvio
				+ ", eventos=" + eventos + "]";
	}
	
	
   
	
}

