package com.carsa.tracking.entidades;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Clase que contiene la información de una Entrega que se intento dar de alta en OCA pero fue rechazada
 * 
 * Un Grupo de Entregas Informado contiene una lista de estas
 * 
 * */
@JsonAutoDetect
public class DetalleEntregaRechazada {

	private String operativa;
	private String nroEntregaSap;
	private String motivo;
	private Integer cantidad;
	
	public String getOperativa() {
		return operativa;
	}
	public void setOperativa(String operativa) {
		this.operativa = operativa;
	}
	public String getNroEntregaSap() {
		return nroEntregaSap;
	}
	public void setNroEntregaSap(String nroEntregaSap) {
		this.nroEntregaSap = nroEntregaSap;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}


}

