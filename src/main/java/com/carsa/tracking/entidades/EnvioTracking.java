package com.carsa.tracking.entidades;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Clase que contiene un Numero de Envio. Este número representa una Entrega dada de alta en OCA
 * 
 * Es utilizada para solicitar la obtencion en OCA del historial de cambios de estados de una Entrega.
 * 
 * */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonAutoDetect
public class EnvioTracking extends EnviosOperation{
	
	@XmlElement(required=true)
	@NotNull
	@NotEmpty
	private String nroEnvio;

	public String getNroEnvio()  {
		return nroEnvio;
	}

	public void setNroEnvio(String nroEnvio) {
		this.nroEnvio = nroEnvio;
	}

	public EnvioTracking(String nroEnvio, String operador) {
		super();
		this.nroEnvio = nroEnvio;
		
		if(operador == null)
			this.setOperador(TipoCourier.Oca);
		else if(operador.equals(TipoCourier.Oca.toString()))
			this.setOperador(TipoCourier.Oca);
		else if(operador.equals(TipoCourier.Andreani.toString()))
			this.setOperador(TipoCourier.Andreani);
		else if(operador.equals(TipoCourier.AndreaniCorreo.toString()))
			this.setOperador(TipoCourier.AndreaniCorreo);
		else if(operador.equals(TipoCourier.CorreoArgentino.toString()))
			this.setOperador(TipoCourier.CorreoArgentino);
		else
			this.setOperador(TipoCourier.Oca);
	}
	
	

	

	public EnvioTracking() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "EnvioTracking [nroEnvio=" + nroEnvio + ", operador="
				+ getOperador() + "]";
	}
	
	

	
	
	

}

