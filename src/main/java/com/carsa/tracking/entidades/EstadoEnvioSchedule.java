package com.carsa.tracking.entidades;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@Document(collection="estadoEnvioSchedule")
@JsonAutoDetect
public class EstadoEnvioSchedule {
	
	@Id
	private String id;
	private String nroGuia;
	private Date fechaInicio;
	private Date fechaProximoSchedule;
	private Integer cantidadEstados;
	private Boolean terminado;
	private String operador;
	
	
	
	public String getNroGuia() {
		return nroGuia;
	}
	public void setNroGuia(String nroGuia) {
		this.nroGuia = nroGuia;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaProximoSchedule() {
		return fechaProximoSchedule;
	}
	public void setFechaProximoSchedule(Date fechaProximoSchedule) {
		this.fechaProximoSchedule = fechaProximoSchedule;
	}
	
	public Integer getCantidadEstados() {
		return cantidadEstados;
	}
	public void setCantidadEstados(Integer cantidadEstados) {
		this.cantidadEstados = cantidadEstados;
	}
	public Boolean getTerminado() {
		return terminado;
	}
	public void setTerminado(Boolean terminado) {
		this.terminado = terminado;
	}
	
	
	
	public String getOperador() {
		return operador;
	}
	public void setOperador(String operador) {
		this.operador = operador;
	}
	@Override
	public String toString() {
		return "EstadoEnvioSchedule [id=" + id + ", nroGuia=" + nroGuia
				+ ", fechaInicio=" + fechaInicio + ", fechaProximoSchedule="
				+ fechaProximoSchedule + ", cantidadEstados=" + cantidadEstados
				+ ", terminado=" + terminado + "]";
	}
	
	
	

}

