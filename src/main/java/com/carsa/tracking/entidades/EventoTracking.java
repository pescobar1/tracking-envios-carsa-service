package com.carsa.tracking.entidades;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class EventoTracking implements Comparable<EventoTracking>
{
	private String nroGuia;
	private String motivo;
	private String estado;
	private String sucursal;
	private String fecha;
	private TipoCourier operador;

	public String getMotivo()
	{
		return motivo;
	}

	public void setMotivo(String motivo)
	{
		this.motivo = motivo;
	}

	public String getEstado()
	{
		return estado;
	}

	public void setEstado(String estado)
	{
		this.estado = estado;
	}

	public String getSucursal()
	{
		return sucursal;
	}

	public void setSucursal(String sucursal)
	{
		this.sucursal = sucursal;
	}

	public String getFecha()
	{
		return fecha;
	}

	public void setFecha(String fecha)
	{
		this.fecha = fecha;
	}

	public String getNroGuia()
	{
		return nroGuia;
	}

	public void setNroGuia(String nroGuia)
	{
		this.nroGuia = nroGuia;
	}

	public TipoCourier getOperador()
	{
		return operador;
	}

	public void setOperador(TipoCourier operador)
	{
		this.operador = operador;
	}

	@Override
	/**
	 * Compara objetos por la fecha
	 */
	public int compareTo(EventoTracking o)
	{
		try {
			Date d = null;
			Date d1 = null;
			Date d2 = null;
						
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			if (this.getOperador() == TipoCourier.Oca){
			
				sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				d = sdf.parse(this.fecha);
				String formattedTime = output.format(d);
				d1 = output.parse(formattedTime);
				
				d = sdf.parse(o.fecha);
				formattedTime = output.format(d);
				d2 = output.parse(formattedTime);
			} else if (this.getOperador() == TipoCourier.CorreoArgentino){
				sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				d = sdf.parse(this.fecha);
				String formattedTime = output.format(d);
				d1 = output.parse(formattedTime);
				
				d = sdf.parse(o.fecha);
				formattedTime = output.format(d);
				d2 = output.parse(formattedTime);
			} else {
				d = sdf.parse(this.fecha);
				String formattedTime = output.format(d);
				d1 = output.parse(formattedTime);
				
				d = sdf.parse(o.fecha);
				formattedTime = output.format(d);
				d2 = output.parse(formattedTime);
			}
			return d1.compareTo(d2);
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		return 0;
	}

}

