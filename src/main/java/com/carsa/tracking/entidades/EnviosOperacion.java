package com.carsa.tracking.entidades;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;


/**
 * Clase de la que extienden las clases que representan una solicitud de una Operación determinada.
 * Contiene un TipoCourier que representa el operador sobre el que se hacen las operaciones
 * Las que extienden de esta son:
 * * GrupoDeEntregas
 * * OrdenRetiroParaAnular
 * * EnvioTracking
 * * SolicitudObtenerEtiquetas
 * */
@XmlTransient
@JsonAutoDetect
abstract class EnviosOperation {
	@XmlElement(required=true)
	private TipoCourier operador;

	public TipoCourier getOperador() {
		return operador;
	}

	public void setOperador(TipoCourier operador) {
		this.operador = operador;
	}
	
}

