package com.carsa.tracking.entidades;

/**
 * OPERADOR
 * */
public enum TipoCourier {
	
	Andreani,
	AndreaniCorreo,
	CorreoArgentino,
	Oca,
	Oca_error;
	
}
