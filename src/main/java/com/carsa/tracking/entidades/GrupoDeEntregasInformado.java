package com.carsa.tracking.entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Clase que contiene la información devuelta por OCA al dar de Alta un Grupo de Entregas. 
 * Contiene una lista de las Entregas Generadas y las Rechazadas.
 * 
 * */

@Document(collection="gruposDeEntregasInformadoOca")
@JsonAutoDetect
public class GrupoDeEntregasInformado {

	@Id
	@XmlTransient
	private String id;
	private String nroGrupo;
	private Integer codigoOperacion;
	private Date fechaIngreso;
	private String usuario;
	private String origen;
	private Integer cantEntregas;
	private Integer cantEntregasGeneradas;
	private Integer cantEntregasRechazadas;
	private List<DetalleEntregaRechazada> rechazadas=new ArrayList<DetalleEntregaRechazada>();
	private List<DetalleEntregaGenerada> generadas= new ArrayList<DetalleEntregaGenerada>();
	
	
	public void addEntregaRechazada(DetalleEntregaRechazada r){
		rechazadas.add(r);
	}
	
	public void addEntregaGenerada(DetalleEntregaGenerada g){
		generadas.add(g);
	}
	
	public Integer getCodigoOperacion() {
		return codigoOperacion;
	}
	public void setCodigoOperacion(Integer codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public Integer getCantEntregas() {
		return cantEntregas;
	}
	public void setCantEntregas(Integer cantEntregas) {
		this.cantEntregas = cantEntregas;
	}
	public Integer getCantEntregasGeneradas() {
		return cantEntregasGeneradas;
	}
	public void setCantEntregasGeneradas(Integer cantEntregasGeneradas) {
		this.cantEntregasGeneradas = cantEntregasGeneradas;
	}
	public Integer getCantEntregasRechazadas() {
		return cantEntregasRechazadas;
	}
	public void setCantEntregasRechazadas(Integer cantEntregasRechazadas) {
		this.cantEntregasRechazadas = cantEntregasRechazadas;
	}
	public List<DetalleEntregaRechazada> getRechazadas() {
		return rechazadas;
	}
	public void setRechazadas(List<DetalleEntregaRechazada> rechazadas) {
		this.rechazadas = rechazadas;
	}
	public List<DetalleEntregaGenerada> getGeneradas() {
		return generadas;
	}
	public void setGeneradas(List<DetalleEntregaGenerada> generadas) {
		this.generadas = generadas;
	}

	
	public String getNroGrupo() {
		return nroGrupo;
	}

	public void setNroGrupo(String nroGrupo) {
		this.nroGrupo = nroGrupo;
	}

	@Override
	public String toString() {
		return "GrupoDeEntregasInformado [codigoOperacion=" + codigoOperacion + ", fechaIngreso=" + fechaIngreso + ", usuario=" + usuario + ", origen="
				+ origen + ", cantEntregas=" + cantEntregas + ", cantEntregasGeneradas=" + cantEntregasGeneradas + ", cantEntregasRechazadas="
				+ cantEntregasRechazadas + ", rechazadas.size=" + rechazadas.size() + ", generadas.size=" + generadas.size() + "]";
	}
	
	

}

