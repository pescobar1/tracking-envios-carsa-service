package com.carsa.tracking.entidades;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Clase que contiene la información de una Entrega que se fue dada de alta correctamente en OCA.
 * 
 * Un Grupo de Entregas Informado contiene una lista de estas
 * 
 * */
@JsonAutoDetect
public class DetalleEntregaGenerada {

	private String operativa;
	private String ordenRetiro;
	private String numeroEnvio;
	private String estado;
	private String nroEntregaSap;
	private String codigoArticulo;
	
	private String zonaReparto;
	private String depositoCabecera;
	private String tipoServicio;

	public String getOperativa() {
		return operativa;
	}
	public void setOperativa(String operativa) {
		this.operativa = operativa;
	}
	public String getOrdenRetiro() {
		return ordenRetiro;
	}
	public void setOrdenRetiro(String ordenRetiro) {
		this.ordenRetiro = ordenRetiro;
	}
	public String getNumeroEnvio() {
		return numeroEnvio;
	}
	public void setNumeroEnvio(String numeroEnvio) {
		this.numeroEnvio = numeroEnvio;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNroEntregaSap() {
		return nroEntregaSap;
	}
	public void setNroEntregaSap(String nroEntregaSap) {
		this.nroEntregaSap = nroEntregaSap;
	}
	public String getCodigoArticulo() {
		return codigoArticulo;
	}
	public void setCodigoArticulo(String codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}
	public String getZonaReparto() {
		return zonaReparto;
	}
	public void setZonaReparto(String zonaReparto) {
		this.zonaReparto = zonaReparto;
	}
	public String getDepositoCabecera() {
		return depositoCabecera;
	}
	public void setDepositoCabecera(String depositoCabecera) {
		this.depositoCabecera = depositoCabecera;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	
	
	
}

