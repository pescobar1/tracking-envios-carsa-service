package com.carsa.tracking.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carsa.tracking.entidades.DetalleEntregaGenerada;
import com.carsa.tracking.entidades.EnvioTrackingResponse;
import com.carsa.tracking.entidades.EstadoEnvioSchedule;
import com.carsa.tracking.entidades.EventoTracking;
import com.carsa.tracking.entidades.GrupoDeEntregasInformado;
import com.carsa.tracking.entidades.TipoCourier;
import com.carsa.tracking.repository.EstadoEnvioScheduleRepository;
import com.carsa.tracking.service.EstadoEnvioScheduleService;

@Service
public class EstadoEnvioScheduleServiceImpl  implements EstadoEnvioScheduleService{

	@Autowired
	EstadoEnvioScheduleRepository estadoEnvioScheduleRepository;
	
	public EstadoEnvioSchedule buscar(String nroGuia)
	{
		return estadoEnvioScheduleRepository.findBynroGuia(nroGuia);
	}
	
	public List<EstadoEnvioSchedule> buscarEstados()
	{
		//SE RETORNARAN SOLO LOS PEDIDOS CUYA FECHA INICIO NO SEA MAYOR A 30 DIAS ANTERIORES
		Calendar fechaDiasAnteriores = Calendar.getInstance();
		fechaDiasAnteriores.add(Calendar.DATE, -30);
		
		List<EstadoEnvioSchedule> estados = estadoEnvioScheduleRepository.findEstadoEnvioSchedulesByFechaProximoSchedule(fechaDiasAnteriores.getTime(), Calendar.getInstance().getTime());//calendar.getTime(),calendar2.getTime());
		
		return estados;	
	}
	
	public void guardarEstadosGrupos(List<GrupoDeEntregasInformado> grupos, String operador)
	{
		for (GrupoDeEntregasInformado grupoDeEntregasInformado : grupos) 
		{
			List<DetalleEntregaGenerada> generadas = grupoDeEntregasInformado.getGeneradas();
			for (DetalleEntregaGenerada detalleEntregaGenerada : generadas) 
			{
				
				EstadoEnvioSchedule estado = new EstadoEnvioSchedule();
				
				Calendar fechaInicio = Calendar.getInstance();
				estado.setFechaInicio(fechaInicio.getTime());
				
				
				Calendar fechaProximoSchedule = Calendar.getInstance();
				fechaProximoSchedule.add(Calendar.HOUR, 2);
				estado.setFechaProximoSchedule(fechaProximoSchedule.getTime());
				
				String nroEnvio = detalleEntregaGenerada.getNumeroEnvio();
				if(operador.equals(TipoCourier.Oca.toString()))
					estado.setNroGuia(nroEnvio);
				else
					estado.setNroGuia(detalleEntregaGenerada.getNumeroEnvio());
				
				estado.setCantidadEstados(0);
				estado.setTerminado(false);
				
				estado.setOperador(operador);
				estadoEnvioScheduleRepository.save(estado);
			}
		}
	}
	
	public void guardar(EstadoEnvioSchedule estado)
	{
		estadoEnvioScheduleRepository.save(estado);
	}
	
	/**
	 * metodo que compara la cantidad de eventos de la lista de eventos obtenida. Si
	 * la cantidad es distinta a la almacenada en el estado schedule, es porque hay
	 * nuevos estados y se deberán guardar los nuevos.
	 * 
	 * @param estadoEnvioSchedule
	 * @param envioTracking
	 * @return
	 */
	public List<EventoTracking> obtenerNuevosEstados(EstadoEnvioSchedule estadoEnvioSchedule, EnvioTrackingResponse envioTracking)
	{
		// Ordeno los eventos de manera ascendente, por fecha (utilizando EventoTracking.compareTo())
		Collections.sort(envioTracking.getEventos());
		
		ArrayList<EventoTracking> nuevosEventos = new ArrayList<EventoTracking>();
		if(estadoEnvioSchedule.getCantidadEstados()<envioTracking.getEventos().size())
		{
			//HAY NUEVOS ESTADOS
			for (int i = estadoEnvioSchedule.getCantidadEstados() ; i < envioTracking.getEventos().size(); i++) {
				EventoTracking evento = envioTracking.getEventos().get(i);
				//evento.setNroGuia(estadoEnvioSchedule.getNroGuia());
				
				nuevosEventos.add(evento);
			}
			
		}
		return nuevosEventos;
		
	}
	
	public void actualizarFechaSchedule(EstadoEnvioSchedule estado)
	{
		if(!estado.getTerminado())
		{
			Calendar fechaProximoSchedule = Calendar.getInstance();
		
			fechaProximoSchedule.setTime(estado.getFechaProximoSchedule());
			
			fechaProximoSchedule.add(Calendar.HOUR, 2);
					
			estado.setFechaProximoSchedule(fechaProximoSchedule.getTime());
		}
		
		guardar(estado);
		
	}
	
	public void actualizarEstado(EstadoEnvioSchedule estadoEnvioSchedule, EnvioTrackingResponse envioTracking)
	{
		estadoEnvioSchedule.setCantidadEstados(envioTracking.getEventos().size());
		
		if(isTerminado(envioTracking.getEventos()))//, envioTracking.getSituacion()))
			estadoEnvioSchedule.setTerminado(true);
	}
	
	public Boolean isTerminado(List<EventoTracking> eventos)//, String situacion)
	{
		for (EventoTracking eventoTracking : eventos) {
			String estadoEvento = eventoTracking.getEstado();
			if(eventoTracking.getOperador().equals(TipoCourier.Oca) &&
					(estadoEvento.contains("Entregado") ||
					estadoEvento.contains("Rendición de entrega en proceso")||
					estadoEvento.contains("Rendición de entrega finalizada")||
					estadoEvento.contains("Acuse de recibo digitalizado") ||
					estadoEvento.contains("Devuelto al Remitente") )
//					estadoEvento.contains("Envío Cancelado")||
//					estadoEvento.contains("Retiro Cancelado") )
				)
			{
				return true;
			}
			
			else if(eventoTracking.getOperador().equals(TipoCourier.Andreani)&&
					(estadoEvento.contains("EXPEDICION ENTREGADA") ||
					 estadoEvento.contains("CONFORME ENTREGADO") ||
					 estadoEvento.contains("RECIBIDO CONFORME EN DESTINO"))){
				return true;
			}
			else if(eventoTracking.getOperador().equals(TipoCourier.AndreaniCorreo)&&
					estadoEvento.contains("Envío entregado")){
				return true;
			}
			else if(eventoTracking.getOperador().equals(TipoCourier.CorreoArgentino) &&
					(estadoEvento.contains("ENT") ||
					 estadoEvento.contains("ENTREGADO") ||
					 estadoEvento.contains("RT_ENT") ||
					 estadoEvento.contains("Devuelto"))){
				return true;
			}
			
		}
		
		return false;
	}
	
	/**
	 * METODO QUE PONE COMO TERMINADO UN SCHEDULE CUANDO UN ENVIO FUE ANULADO
	 * 
	 * @param nroGuia
	 */
	public void setearTerminado(String nroGuia)
	{
		EstadoEnvioSchedule estado = estadoEnvioScheduleRepository.findBynroGuia(nroGuia);
		estado.setTerminado(true);
		estadoEnvioScheduleRepository.save(estado);
	}
	
	/**
	 * METODO QUE CREA UN EVENTO DE ANULACION 
	 * 
	 * @param estado
	 * @return
	 */
	public EventoTracking crearEventoAnulacion(EstadoEnvioSchedule estado){
		EventoTracking eventoAnulado = new EventoTracking();
		eventoAnulado.setNroGuia(estado.getNroGuia());
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Calendar fecha = Calendar.getInstance();
		
		
		eventoAnulado.setFecha(formatter.format(fecha.getTime()));
		eventoAnulado.setMotivo("Sin Motivo");
		eventoAnulado.setEstado("Anulado");
		eventoAnulado.setSucursal("");
		
		if(estado.getOperador()!=null && estado.getOperador().equals("Andreani"))
			eventoAnulado.setOperador(TipoCourier.Andreani);
		else if (estado.getOperador()!=null && estado.getOperador().equals("AndreaniCorreo"))
			eventoAnulado.setOperador(TipoCourier.AndreaniCorreo);
		else if (estado.getOperador()!=null && estado.getOperador().equals("CorreoArgentino"))
			eventoAnulado.setOperador(TipoCourier.CorreoArgentino);
		else 
			eventoAnulado.setOperador(TipoCourier.Oca);
		
		return eventoAnulado;
	}

	@Override
	public List<EstadoEnvioSchedule> buscarEstadosViejos() {
		//SE RETORNARAN SOLO LOS PEDIDOS CUYA FECHA INICIO NO SEA MAYOR A 30 DIAS ANTERIORES
		Calendar fecha = Calendar.getInstance();
		fecha.add(Calendar.DATE, -(30));
				
		//List<EstadoEnvioSchedule> estados = estadoEnvioScheduleRepository.findEstadoEnvioSchedulesByFechaProximoSchedule(fechaInicio, fechaFin, fechaSieteDiasAnteriores.getTime());//calendar.getTime(),calendar2.getTime());
		List<EstadoEnvioSchedule> estados = estadoEnvioScheduleRepository.findEstadoEnvioSchedulesViejos(fecha.getTime());//calendar.getTime(),calendar2.getTime());
				
		return estados;	
	}
	
	@Override
	public EstadoEnvioSchedule actualizarEstadoViejos(EstadoEnvioSchedule estado) {
		
		if(!estado.getTerminado()){
			estado.setTerminado(true);
			estadoEnvioScheduleRepository.save(estado);
		}
		
		return estado;	
	}
	
}
 
