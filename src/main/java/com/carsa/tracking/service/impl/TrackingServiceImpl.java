package com.carsa.tracking.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.carsa.customer.tracer.dto.CustomerEventDto;
import com.carsa.tracking.entidades.EnvioTracking;
import com.carsa.tracking.entidades.EnvioTrackingResponse;
import com.carsa.tracking.entidades.EstadoEnvioSchedule;
import com.carsa.tracking.entidades.EventoTracking;
import com.carsa.tracking.service.CustomerEventService;
import com.carsa.tracking.service.EstadoEnvioScheduleService;
import com.carsa.tracking.service.TrackingService;
import com.carsa.tracking.util.RequestGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Service
public class TrackingServiceImpl implements TrackingService {
	
	private static final Logger LOG = LogManager.getLogger(TrackingServiceImpl.class);
	
	@Autowired
	EstadoEnvioScheduleService estadoEnvioScheduleService;
	
	@Autowired
	RequestGenerator requestGenerator;
	
	@Autowired 
	CustomerEventService customerEventService;
	
	@Value("${envios.carsa.url}")
	String enviosCarsaUrl;
	
	@Value("${envios.tracking.url}")
	String enviosTrackingUrl;
	
	@Value("${business.event.url}")
	String businessEventUrl;
	
	@Scheduled(fixedDelayString = "${schedule.fixed.delay.actualizar}")
	public void actualizarEstadosEnvio() {
		List<EstadoEnvioSchedule> estadosEnvio = new ArrayList<EstadoEnvioSchedule>();
		estadosEnvio = estadoEnvioScheduleService.buscarEstados();
		estadosEnvio.forEach((estado)->{
			buscarActualizarNuevosEstados(estado);
		});

	}
	
	public void buscarActualizarNuevosEstados(EstadoEnvioSchedule estado) {
		EnvioTracking envioTracking = new EnvioTracking(estado.getNroGuia(),estado.getOperador());
		Gson gson = new Gson();
		String jsonEnvio = gson.toJson(envioTracking);
		String params = "services1/EnviosServicesRest/trackingHistoricoEnvio";
		String res = requestGenerator.post(enviosCarsaUrl, params, jsonEnvio);
		LOG.info(res);
		EnvioTrackingResponse envioTrackingResponse = new Gson().fromJson(res, EnvioTrackingResponse.class);
		List<EventoTracking> eventosTracking = estadoEnvioScheduleService.obtenerNuevosEstados(estado, envioTrackingResponse);
		eventosTracking.forEach((evento)->{
			try {
				guardarEstado(evento);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//enviarBussisnesEvent() recibe de amq, es necesario llamarlo?
		});
		estadoEnvioScheduleService.actualizarEstado(estado,envioTrackingResponse);
		estadoEnvioScheduleService.actualizarFechaSchedule(estado);
	}
	
	public void guardarEstado(EventoTracking evento) throws Exception {
		Gson gson = new Gson();
		String jsonEvento = gson.toJson(evento);
		String params = "flete-externo-webapp/api/setTrackingByGuia";
		String res = requestGenerator.post(enviosTrackingUrl, params, jsonEvento);
		if(res == "true") {
			LOG.info("Evento guardado correctamente. Resultado: ", res);
		}else {
			LOG.info("No se pudo guardar el evento. Resultado: ", res);
			throw new Exception("No se pudo guardar el evento");
		}
	}
	
	public void enviarBusinessEvent(Map<String, String> message) {
		ObjectMapper mapper = new ObjectMapper();
		EventoTracking eventoTracking = mapper.convertValue(message, EventoTracking.class); 
		CustomerEventDto customerEventDto = customerEventService.crearBusinessEvent(eventoTracking);
		Gson gson = new Gson();
		String jsonEventDto = gson.toJson(customerEventDto);
		LOG.info("ESTADO ENVIADO A BUSINESS: ", jsonEventDto);
		String params = "business/event";
		String res = requestGenerator.post(businessEventUrl, params, jsonEventDto);
		LOG.info("Respuesta del envio evento: ", res);
	}
	
	@Scheduled(fixedDelayString = "${schedule.fixed.delay.limpiar}")
	public void limpiarScheduleViejos() {
		List<EstadoEnvioSchedule> estadosEnvioViejos = estadoEnvioScheduleService.buscarEstadosViejos();
		LOG.info("Cantidad: " + estadosEnvioViejos.size());
		estadosEnvioViejos.forEach((estado)->{
			LOG.info("Aqui actualiza estados");
			estadoEnvioScheduleService.actualizarEstadoViejos(estado);
			
		});
	}
	
}
