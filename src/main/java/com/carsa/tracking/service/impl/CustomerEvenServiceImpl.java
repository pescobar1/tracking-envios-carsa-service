package com.carsa.tracking.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.carsa.customer.tracer.dto.BusinessEventType;
import com.carsa.customer.tracer.dto.BusinessProcessName;
import com.carsa.customer.tracer.dto.CustomerEventDto;
import com.carsa.customer.tracer.dto.CustomerEventDto.CustomerEventSource;
import com.carsa.customer.tracer.dto.CustomerEventDto.Shipping;
import com.carsa.customer.tracer.dto.IdentifierType;
import com.carsa.tracking.entidades.EventoTracking;
import com.carsa.tracking.entidades.TipoCourier;
import com.carsa.tracking.service.CustomerEventService;

@Service(value = "customerEvenService")
public class CustomerEvenServiceImpl implements CustomerEventService{
	
	public CustomerEventDto crearBusinessEvent(EventoTracking eventoTracking)
	{
		String estadoUltimoEvento = eventoTracking.getEstado();
		
		
		CustomerEventDto customerEvent = new CustomerEventDto();
		if(eventoTracking.getOperador().equals(TipoCourier.Oca))
		{
			if((estadoUltimoEvento.toLowerCase().contains("entregado") && !estadoUltimoEvento.toLowerCase().contains("no entregado")) ||
					estadoUltimoEvento.contains("Rendici�n de entrega en proceso")||
					estadoUltimoEvento.contains("Rendici�n de entrega finalizada")||
					estadoUltimoEvento.contains("Acuse de recibo digitalizado"))
			{
				customerEvent.setEventType(BusinessEventType.ENTREGA_REALIZADA);
			}
			else if(estadoUltimoEvento.contains("Anulado") ||
					estadoUltimoEvento.contains("Devuelto al Remitente"))
//					estadoUltimoEvento.contains("Env�o Cancelado") ||
//					estadoUltimoEvento.contains("Retiro Cancelado") )
			{
				customerEvent.setEventType(BusinessEventType.ENTREGA_CANCELADA);
			}
			else
			{
				customerEvent.setEventType(BusinessEventType.ENTREGA_EN_TRANSPORTE);
			}
		}
		else if(eventoTracking.getOperador().equals(TipoCourier.Andreani))
		{
			if(estadoUltimoEvento.contains("EXPEDICION ENTREGADA") ||
			   estadoUltimoEvento.contains("CONFORME ENTREGADO") ||
			   estadoUltimoEvento.contains("RECIBIDO CONFORME EN DESTINO"))
				customerEvent.setEventType(BusinessEventType.ENTREGA_REALIZADA);
			else
				customerEvent.setEventType(BusinessEventType.ENTREGA_EN_TRANSPORTE);
		}
		else if(eventoTracking.getOperador().equals(TipoCourier.AndreaniCorreo))
		{
			if(estadoUltimoEvento.contains("Env�o entregado")){
				if(estadoUltimoEvento.contains("al remitente"))
					customerEvent.setEventType(BusinessEventType.ENTREGA_CANCELADA);
				else
					customerEvent.setEventType(BusinessEventType.ENTREGA_REALIZADA);
			}
			else
				customerEvent.setEventType(BusinessEventType.ENTREGA_EN_TRANSPORTE);
		}
		if(eventoTracking.getOperador().equals(TipoCourier.CorreoArgentino))
		{
			if(estadoUltimoEvento.contains("ENTREGADO") || 
			   estadoUltimoEvento.contains("ENT"))
				customerEvent.setEventType(BusinessEventType.ENTREGA_REALIZADA);
			else if(estadoUltimoEvento.contains("No Entregado - DIRECCION INEXISTENTE") ||
					estadoUltimoEvento.contains("RT_NEN-DI") ||
					estadoUltimoEvento.contains("No Entregado - SE MUDO EL DESTINATARIO") ||
					estadoUltimoEvento.contains("RT_NEN-SM") ||
					estadoUltimoEvento.contains("No Entregado - RECHAZADO") ||
					estadoUltimoEvento.contains("RT_NEN-RE") ||
					estadoUltimoEvento.contains("No Entregado - DIRECCION INSUFICIENTE") ||
					estadoUltimoEvento.contains("RT_NEN-DF") ||
					estadoUltimoEvento.contains("No Entregado - DESCONOCIDO") ||
					estadoUltimoEvento.contains("RT_NEN-DE") ||
					estadoUltimoEvento.contains("No Entregado - FALLECIDO") ||
					estadoUltimoEvento.contains("RT_NEN-FA") ||
					estadoUltimoEvento.contains("No Entregado - DIRECCION INACCESIBLE") ||
					estadoUltimoEvento.contains("RT_NEN-DC"))
			{
				customerEvent.setEventType(BusinessEventType.ENTREGA_CANCELADA);
			}
			else 
			{
				customerEvent.setEventType(BusinessEventType.ENTREGA_EN_TRANSPORTE);
			}
		}
		
		Shipping shipping= new Shipping();
		shipping.setShippingCompany(eventoTracking.getOperador().toString());
		customerEvent.setShipping(shipping);
		
		customerEvent.setIdentifierType(IdentifierType.NRO_GUIA_TRANSPORTE);
		customerEvent.setIdentifierValue(eventoTracking.getNroGuia());
		customerEvent.setReferenceIdentifierType(IdentifierType.NRO_GUIA_TRANSPORTE);
		customerEvent.setReferenceIdentifierValue(eventoTracking.getNroGuia());
		customerEvent.setEventMessage(eventoTracking.getEstado());
		
	 
	
		try {
			Date fecha = null;
			if (eventoTracking.getOperador().equals(TipoCourier.CorreoArgentino)){
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				Date d = sdf.parse(eventoTracking.getFecha());
				String formattedTime = output.format(d);
				fecha = output.parse(formattedTime);
			} else if (eventoTracking.getOperador().equals(TipoCourier.Oca)){
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				fecha = formatter.parse(eventoTracking.getFecha());
			} else {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				fecha = formatter.parse(eventoTracking.getFecha());
			}
			
			customerEvent.setEventDate(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		CustomerEventSource eventSource = new CustomerEventSource();
		eventSource.setSystem("ESB");
		eventSource.setProcess(BusinessProcessName.VENTA_MCOM);
		customerEvent.setCustomerEventSource(eventSource);
		
		return customerEvent;
	}

}
