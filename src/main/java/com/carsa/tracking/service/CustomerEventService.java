package com.carsa.tracking.service;

import com.carsa.customer.tracer.dto.CustomerEventDto;
import com.carsa.tracking.entidades.EventoTracking;

public interface CustomerEventService {
	public CustomerEventDto crearBusinessEvent(EventoTracking eventoTracking);
}
