package com.carsa.tracking.service;

import java.util.Date;
import java.util.List;

import com.carsa.tracking.entidades.EnvioTrackingResponse;
import com.carsa.tracking.entidades.EstadoEnvioSchedule;
import com.carsa.tracking.entidades.EventoTracking;
import com.carsa.tracking.entidades.GrupoDeEntregasInformado;


public interface EstadoEnvioScheduleService {

	public EstadoEnvioSchedule buscar(String nroGuia);
	public List<EstadoEnvioSchedule> buscarEstados();
	public void guardarEstadosGrupos(List<GrupoDeEntregasInformado> grupos, String operador);
	public void guardar(EstadoEnvioSchedule estado);
	public List<EstadoEnvioSchedule> buscarEstadosViejos();
	public EstadoEnvioSchedule actualizarEstadoViejos(EstadoEnvioSchedule estado);
	public void actualizarEstado(EstadoEnvioSchedule estadoEnvioSchedule, EnvioTrackingResponse envioTracking);
	public void actualizarFechaSchedule(EstadoEnvioSchedule estado);
	public List<EventoTracking> obtenerNuevosEstados(EstadoEnvioSchedule estadoEnvioSchedule, EnvioTrackingResponse envioTracking);
}
