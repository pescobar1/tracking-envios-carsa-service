package com.carsa.tracking.service;

import java.util.Map;

public interface TrackingService {
	
	public void enviarBusinessEvent(Map<String, String> message);
	public void limpiarScheduleViejos();
}
