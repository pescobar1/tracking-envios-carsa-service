package com.carsa.tracking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.carsa.tracking.service.TrackingService;

@RestController
@RequestMapping("/")
public class TrackingController {
	
	@Autowired
	TrackingService trackingService;
	
	@RequestMapping(value = "run", method = RequestMethod.GET)
	public void envioComprobanteRumbo() {
		trackingService.limpiarScheduleViejos();
	}
}
