package com.carsa.tracking.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.carsa.tracking.entidades.EstadoEnvioSchedule;

@Repository
public interface EstadoEnvioScheduleRepository extends PagingAndSortingRepository<EstadoEnvioSchedule,String>{

	@Query(value="{'terminado':false , 'fechaProximoSchedule':{'$gt': ?0}, 'fechaProximoSchedule':{'$lt': ?1}}")
	List<EstadoEnvioSchedule> findEstadoEnvioSchedulesByFechaProximoSchedule(Date fechaInicio, Date fechaFin);
	
	@Query(value="{'terminado':false, 'fechaProximoSchedule':{'$lt': ?0}, $or: [{'fechaInicio':{'$lt': ?0}}]}")
	List<EstadoEnvioSchedule> findEstadoEnvioSchedulesViejos(Date fecha);
	
	EstadoEnvioSchedule findBynroGuia(String nroGuia);
}
