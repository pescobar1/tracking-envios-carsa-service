package com.carsa.tracking.listener;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

import com.carsa.tracking.service.TrackingService;

public class MessageListener {
	
	private static final Logger LOG = LogManager.getLogger(MessageListener.class);
	
	@Autowired
	TrackingService trackingService;
	
	@JmsListener(destination = "${queue.tracking.evento.anulacion}")
    public void receiveMessage(Map<String, String> message) {
        LOG.info("Received <" + message + ">");
        trackingService.enviarBusinessEvent(message);
    }
}
