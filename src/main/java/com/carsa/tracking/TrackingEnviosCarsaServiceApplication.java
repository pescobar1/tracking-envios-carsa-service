package com.carsa.tracking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages={"com.carsa.tracking"})
@EnableScheduling
public class TrackingEnviosCarsaServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackingEnviosCarsaServiceApplication.class, args);
	}

}
